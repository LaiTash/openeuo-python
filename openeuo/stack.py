"""

uo.dll stack communication module

"""

__author__ = 'Lai Tash'
__email__ = 'lai.tash@gmail.com'
__license__ = "GPL"


from ctypes import *
from ctypes.wintypes import *
from threading import Lock



_DLL_NAME_DEFAULT = 'uo'

class StackError(Exception):
    pass


class BaseStack(object):
    """Low-level stack representation"""
    def __init__(self, dll_name=_DLL_NAME_DEFAULT):
        self.dll = WinDLL('uo')
        self.dll.GetBoolean.restype = BOOL
        self.dll.GetString.restype = c_char_p
        self.dll.GetDouble.restype = c_double
        
        self.hnd = self.dll.Open()

    @property
    def dll_version(self):
        return self.dll.Version()

    def Close(self):
        self.dll.Close(self.hnd)
        self.hnd = None

    def _check_index(self, idx):
        if idx > self.GetTop():
            raise IndexError('%d not in stack' % idx)

    def Execute(self):
        return self.dll.Execute(self.hnd)

    def Query(self):
        return self.dll.Query(self.hnd)

    def PushNil(self):
        return self.dll.PushNil(self.hnd)

    def PushBoolean(self, value):
        return self.dll.PushBoolean(self.hnd, BOOL(bool(value)))

    def PushInteger(self, value):
        return self.dll.PushInteger(self.hnd, c_int(int(value)))

    def PushDouble(self, value):
        return self.dll.PushDouble(self.hnd, c_double(float(value)))

    def PushStrRef(self, value):
        """Push a string reference and return the referenced buffer"""
        result = create_string_buffer(str(value))
        self.dll.PushStrRef(self.hnd, result)
        return result

    def PushStrVal(self, value):
        value = str.encode(str(value))
        return self.dll.PushStrVal(self.hnd, c_char_p(value))

    def GetBoolean(self, idx):
        self._check_index(idx)
        return bool(self.dll.GetBoolean(self.hnd, idx))

    def GetInteger(self, idx):
        self._check_index(idx)
        return int(self.dll.GetInteger(self.hnd, idx))

    def GetDouble(self, idx):
        self._check_index(idx)
        return float(self.dll.GetDouble(self.hnd, idx))

    def GetString(self, idx):
        self._check_index(idx)
        return str(self.dll.GetString(self.hnd, idx))

    def GetTop(self):
        return self.dll.GetTop(self.hnd)

    def GetType(self, idx):
        self._check_index(idx)
        return self.dll.GetType(self.hnd, idx)

    def SetTop(self, idx):
        self._check_index(idx)
        return self.dll.SetTop(self.hnd, idx)

    def Insert(self, idx):
        self._check_index(idx)
        return self.dll.Insert(self.hnd, idx)

    def PushValue(self, idx):
        self._check_index(idx)
        return self.dll.PushValue(self.hnd, idx)

    def Remove(self, idx):
        self._check_index(idx)
        return self.dll.Remove(self.hnd, idx)

    def Mark(self):
        return self.dll.Mark(self.hnd)

    def Clean(self):
        return self.dll.Clean(self.hnd)

    def ListRVars(self):
        return self.dll.ListRVars(self.hnd)

    def GetVarHelp(self, vname):
        return self.dll.GetVarHelp(self.hnd, vname)

    def GetFeatures(self):
        return self.dll.GetFeatures(self.hnd)

    def __del__(self):
        if self.hnd is not None:
            if self.dll is None:
                raise RuntimeError('self.dll freed before stack could be closed')
            self.Close()



class Stack(BaseStack):
    def __init__(self):
        super(Stack, self).__init__()
        self.lock = Lock()
        self.vtypes = (
            None,
            self.GetBoolean,
            None,
            self.GetInteger,
            self.GetString
        )

    def _call(self, method, *args):
        self.top = 0
        self.push(method)
        for arg in args:
            if arg is not None:
                self.push(arg)
        result = self.Execute()
        if not result:
            if len(self) == 1: return self[0]
            elif len(self) == 0: return None
            else: return tuple(self)
        else:
            raise StackError('Error while executing %s (%i)' % (method, result))

    def execute(self, method, *args):
        with self.lock:
            result = self._call(method, *args)
            return result

    def push(self, value):
        if isinstance(value, bool):
            return self.PushBoolean(value)
        elif isinstance(value, int):
            return self.PushInteger(value)
        elif isinstance(value, float):
            return self.PushDouble(value)
        elif isinstance(value, str):
            return self.PushStrVal(value)
        else:
            raise StackError('Cannot push value %s: incompatible type' % value)

    def get_value(self, idx, vtype=None):
        idx = idx + 1
        if idx > self.top:
            raise IndexError('Index %d not on stack' % idx)
        if vtype is None:
            dt = self.dll.GetType(self.hnd, idx)
            if dt == 0:
                return None
            if dt == 2 or dt > 4:
                raise TypeError('Invalid type: %d' % dt)
            vtype = self.vtypes[dt]
        return vtype(idx)

    def __getitem__(self, item):
        return self.get_value(item)

    @property
    def top(self):
        return self.dll.GetTop(self.hnd)

    @top.setter
    def top(self, idx):
        self.dll.SetTop(self.hnd, idx)

    def __len__(self):
        return self.GetTop()

    def __del__(self):
        self.Close()